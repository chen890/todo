<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Todo;

class Todo extends Model
{
    // database fields to be allowed for massive assignment 
    protected $fillable = 
    [
        'title' , 'status',
    ];




    // פונקציה ציבורית בשם משתמשים
   public function user()
   {
       // החזר
       return ($this->belongsTo('App\User'));
   } 
}       

?>
