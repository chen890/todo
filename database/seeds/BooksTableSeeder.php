<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
        [
            [
                'id' => 1,
                'title' => 'Harry Poter',
                'author' => 'J.K Rolling',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => '5', 
            ],
            [
                'title' => 'Game of Thrones',
                'id' => 2,
                'author' => 'George Raymond Richard Martin',
                'created_at' => date('Y-m-d G:i:s'), 
                'user_id' => '1',  
            ],
            [
                'title' => 'Rich dad, Poor dad',
                'id' => 3,
                'author' => 'Robert Kiyosaki',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => '3', 
            ],
            [
                'title' => 'Asymmetry',
                'id' => 4,
                'author' => 'Lisa Halliday',
                'created_at' => date('Y-m-d G:i:s'),   
                'user_id' => '2',  
            ],
            [
                'title' => 'Educated: A Memoir',
                'id' => 5,
                'author' => 'Tara Westover',
                'created_at' => date('Y-m-d G:i:s'),
                'user_id' => '4', 
            ],    
        ]);
    }
}
