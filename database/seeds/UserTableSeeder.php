<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'Jack',
                    'email' => 'jack@gmail.com',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                    
    
                ],
                [
                    'name' => 'John',
                    'email' => 'john@hotmail.com',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                   
                ],
                [
                    'name' => 'Mike',
                    'email' => 'mike@binary.com',
                    'password' => '1234',
                    'created_at' => date('Y-m-d G:i:s'),
                   
                ],
                
            ]);
    }
}
