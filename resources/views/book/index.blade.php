@extends('layouts.app')
@section('content')


<h1> Your Book List: </h1>
<head>
    <style>
        table, th, td{border: 1px solid black;}
    </style>
<table>

    <tr>
        <th> Book ID </th>
        <th> Title </th>
        <th> Author </th>
         <th> Read This Book? </th>
    </tr>

    @foreach($books as $book)
    <tr>
        <td>{{$book->id}}</td>
        <td><a href="{{route('book.edit' , $book->id)}}" >{{$book->title}}</td>
        <td> <a href="{{route('book.edit' , $book->id)}}" >{{$book->author}}</td>
        <td> 
            <a href="{{route('book.edit' , $book->id)}}">
                @if ($book-> status)
                <input type="checkbox" id="{{($book->id)}}" checked >
                @else
                <input type="checkbox" id="{{($book->id)}}">
                @endif
        </td>
    </tr>
    @endforeach
    
</table>
<br>
<a href="{{route('book.create')}}"> Create a new book raw </a>

@endsection