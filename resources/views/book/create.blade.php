@extends('layouts.app')
@section('content')



<h1>
    Create new book:
</h1>

<form method='post' action="{{action('BookController@store')}}">
{{csrf_field()}}
<div class="form-group">

    <label for="title"> Name of the book: </label>
    <input type="text" class ="form-control" name='title'>
    <br>
    <label for="author"> Name of the author: </label>
    <input type="text" class ="form-control" name='author'>
    <br>
    
</div>
<br>
<div class="form-group">
    <input type="submit" class ="form-control" name='submit' value="Save" >
</div>


</form>

@endsection