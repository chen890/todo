@extends('layouts.app')
@section('content')

<h1>
   Edit Book Info:
</h1>

<!-- עריכת ספר -->
<form method='post' action="{{action('BookController@update', $book->id)}}">
    @csrf
    @method('PATCH')  

    <div class="form-group">

        <br>
        
<!-- מראה את מה שיש במסד נתונים לגבי סטטוס הספר- האם נקרא או לא 
        @if ($book-> status)
        <input type="checkbox" id="{{($book->id)}}" checked>
        @else
        <input type="checkbox" id="{{($book->id)}}">
        @endif 
 עד פה -->
        Did you read this book? <select>
                                    <option name="1" value="1">Yes</option>
                                    <option name="0" value="0">No</option>
                                </select>
        <br>
        <br>
     
        <input type="text" class ="form-control" name='title' value= "{{$book->title}}">
        <input type="text" class ="form-control" name='author' value= "{{$book->author}}">
        <input hidden type="text" class ="form-control" name='status' value= "{{$book->title}}">
    </div>
    <div class="form-group">
        <input type="submit" class ="form-control" name='submit' value="Update">
    </div>

</form>



<!-- מחיקת משימה -->
<form method='post' action="{{action('BookController@destroy', $book->id)}}">
    @csrf
    @method('DELETE')
    <div class="form-group">
        <input type="submit" class ="form-control" name='submit' value="Delete">
    </div>
</form>

@endsection