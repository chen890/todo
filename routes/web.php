<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/customers/{id?}', function ($id=null) 
{
    if($id!=null)
    {
        return 'Hello customer number '.$id;
    }
    else
    {
        return 'Hello, No customer was provided';
    }
   
})->name('customers');

Route::resource('todos', 'TodoController')->middleware('auth');
Route::resource('book', 'BookController')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
